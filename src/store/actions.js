import axios from 'axios';

export const INCREASE = 'INCREASE';
export const DECREASE = 'DECREASE';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';

export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_FAILURE = 'FETCH_COUNTER_FAILURE';

export const SAVE_COUNTER_REQUEST = 'SAVE_COUNTER_REQUEST';
export const SAVE_COUNTER_SUCCESS = 'SAVE_COUNTER_SUCCESS';
export const SAVE_COUNTER_FAILURE = 'SAVE_COUNTER_FAILURE';

export const FETCH_TASK_REQUEST = 'FETCH_TASK_REQUEST';
export const FETCH_TASK_SUCCESS = 'FETCH_TASK_SUCCESS';
export const FETCH_TASK_FAILURE = 'FETCH_TASK_FAILURE';

export const SEND_TASK_REQUEST = 'SEND_TASK_REQUEST';
export const SEND_TASK_SUCCESS = 'SEND_TASK_SUCCESS';
export const SEND_TASK_FAILURE = 'SEND_TASK_FAILURE';

export const DELETE_TASK_REQUEST = 'DELETE_TASK_REQUEST';
export const DELETE_TASK_SUCCESS = 'DELETE_TASK_SUCCESS';
export const DELETE_TASK_FAILURE = 'DELETE_TASK_FAILURE';

export const CHANGE = 'CHANGE';// action type

export const increase = () => ({type: INCREASE});
export const decrease = () => ({type: DECREASE});
export const add = value => ({type: ADD, payload: value});
export const subtract = value => ({type: SUBTRACT, payload: value});

export const fetchCounterRequest = () => ({type: FETCH_COUNTER_REQUEST});
export const fetchCounterSuccess = counter => ({type: FETCH_COUNTER_SUCCESS, payload: counter});
export const fetchCounterFailure = () => ({type: FETCH_COUNTER_FAILURE});

export const saveCounterRequest = () => ({type: SAVE_COUNTER_REQUEST});
export const saveCounterSuccess = counter => ({type: SAVE_COUNTER_SUCCESS, payload: counter});
export const saveCounterFailure = () => ({type: SAVE_COUNTER_FAILURE});

export const fetchTaskRequest = () => ({type: FETCH_TASK_REQUEST});
export const fetchTaskSuccess = tasks => ({type: FETCH_TASK_SUCCESS, payload: tasks});
export const fetchTaskFailure = () => ({type: FETCH_TASK_FAILURE});

export const sendTaskRequest = () => ({type: SEND_TASK_REQUEST});
export const sendTaskSuccess = task => ({type: SEND_TASK_SUCCESS, payload: task});
export const sendTaskFailure = () => ({type: SEND_TASK_FAILURE});

export const deleteTaskRequest = () => ({type: DELETE_TASK_REQUEST});
export const deleteTaskSuccess = tasks => ({type: DELETE_TASK_SUCCESS, payload: tasks});
export const deleteTaskFailure = () => ({type: DELETE_TASK_FAILURE});

export const changeVal = task => ({type: CHANGE, payload: task});// action

export const fetchCounter = () => {
  return async (dispatch) => {
    dispatch(fetchCounterRequest());
    try {
      const response = await axios.get('https://jibek-asanova-default-rtdb.firebaseio.com/counter.json');
      const counterData = Object.keys(response.data).map(id => {
          return response.data[id];
      });

      const counter = counterData[counterData.length - 1].counter;
        dispatch(fetchCounterSuccess(counter))
        if (response.data === null) {
        dispatch(fetchCounterSuccess(0));
      } else {
        dispatch(fetchCounterSuccess(counter));
      }
    } catch (e) {
      dispatch(fetchCounterFailure());
    }
  };
};

export const saveCounter = (counter) => {
    return async (dispatch) => {
        try {
            dispatch(saveCounterRequest());
            const response = await axios.post('https://jibek-asanova-default-rtdb.firebaseio.com/counter.json', {counter: counter});
            if(response.data === null) {
                dispatch(saveCounterSuccess())
            }
        } catch (e) {
            dispatch(saveCounterFailure());
        }
    };
};

export const changeInput = (e) => {
    return dispatch => dispatch(changeVal(e.target.value));
}

export const sendTasks = () => {
    return async (dispatch, getState) => {
        dispatch(sendTaskRequest());
        try {
            const task = getState().task;
            const response = await axios.post('https://jibek-asanova-default-rtdb.firebaseio.com/tasks.json', {task: task});
            dispatch(sendTaskSuccess(response.data));
            dispatch(getTasks());
        } catch (e) {
            dispatch(sendTaskFailure());
        }
    }
};

export const getTasks = () => {
    return async (dispatch) => {
        dispatch(fetchTaskRequest());
        try {
            const response = await axios.get('https://jibek-asanova-default-rtdb.firebaseio.com/tasks.json');
            dispatch(fetchTaskSuccess(response.data));
        } catch (e) {
            dispatch(fetchTaskFailure());
        }
    }
};

export const removeTask = (id) => {
    return async (dispatch) => {
        dispatch(deleteTaskRequest());
        try {
            const response = await axios.delete('https://jibek-asanova-default-rtdb.firebaseio.com/tasks/' + id + '.json');
            dispatch(deleteTaskSuccess(response.data));
            dispatch(getTasks());

        } catch (e) {
            dispatch(deleteTaskFailure());
        }
    }
};



