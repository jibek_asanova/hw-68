import {
  ADD, CHANGE,
  DECREASE, DELETE_TASK_FAILURE, DELETE_TASK_REQUEST, DELETE_TASK_SUCCESS,
  FETCH_COUNTER_FAILURE,
  FETCH_COUNTER_REQUEST,
  FETCH_COUNTER_SUCCESS, FETCH_TASK_FAILURE, FETCH_TASK_REQUEST, FETCH_TASK_SUCCESS,
  INCREASE, SAVE_COUNTER_FAILURE, SAVE_COUNTER_REQUEST, SAVE_COUNTER_SUCCESS, SEND_TASK_REQUEST, SEND_TASK_SUCCESS,
  SUBTRACT
} from "./actions";

const initialState = {
  counter: 0,
  loading: false,
  error: null,
  tasks: [],
  task: ''
};

const add = (state, action) => {
  return {...state, counter: state.counter + action.payload};
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREASE:
      return {...state, counter: state.counter + 1};
    case DECREASE:
      return {...state, counter: state.counter - 1};
    case ADD:
      return add(state, action);
    case SUBTRACT:
      return {...state, counter: state.counter - action.payload};
    case FETCH_COUNTER_REQUEST:
      return {...state, error: null, loading: true};
    case FETCH_COUNTER_SUCCESS:
      return {...state, loading: false, counter: action.payload};
    case FETCH_COUNTER_FAILURE:
      return {...state, loading: false, error: action.payload};
    case SAVE_COUNTER_REQUEST:
      return {...state, error: null, loading: true};
    case SAVE_COUNTER_SUCCESS:
      return {...state, loading: false};
    case SAVE_COUNTER_FAILURE:
      return {...state, loading: false};
    case CHANGE:
      return {...state, task: action.payload};
    case FETCH_TASK_REQUEST:
      return {...state, loading: true};
    case FETCH_TASK_SUCCESS:
      if (action.payload === null) {
        return {...state, loading: false, tasks: []}
      } else {
        return {...state, loading: false, tasks: Object.keys(action.payload).map(task => ({
            id: task, ...action.payload[task]
          }))};
      }
    case FETCH_TASK_FAILURE:
      return {...state, loading: false};
    case SEND_TASK_REQUEST:
      return {...state, loading: true};
    case SEND_TASK_SUCCESS:
      return {...state, loading: false, task: ''};
    case DELETE_TASK_REQUEST:
      return {...state, loading: true};
    case DELETE_TASK_SUCCESS:
      return {...state, loading: false};
    case DELETE_TASK_FAILURE:
      return {...state, loading: false}
    default:
      return state;
  }
}

export default reducer;