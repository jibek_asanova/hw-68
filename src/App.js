import Counter from "./containers/Counter/Counter";
import TaskApp from "./containers/TaskApp/TaskApp";
import {BrowserRouter, NavLink, Route, Switch} from "react-router-dom";

const App = () => {
  return (
      <BrowserRouter>
          <NavLink exact to="/" className="nav-link" >Counter</NavLink><br/>
          <NavLink to="/taskApp" className="nav-link" >Task</NavLink>
          <Switch>
              <Route path="/" exact component={Counter}/>
              <Route path="/taskApp" component={TaskApp}/>
          </Switch>
      </BrowserRouter>

  );
};

export default App;
