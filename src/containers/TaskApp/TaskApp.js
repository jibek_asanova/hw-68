import React, {useEffect} from 'react';
import deleteIcon from "../../assets/delete.png";
import './TaskApp.css';
import {useDispatch, useSelector} from "react-redux";
import {changeInput, getTasks, removeTask, sendTasks} from "../../store/actions";
import Spinner from "../../UI/Spinner/Spinner";

const TaskApp = () => {
    const dispatch = useDispatch();
    const tasks1 = useSelector(state => state.tasks);
    const task1 = useSelector(state => state.task);
    const loadingSpinner = useSelector(state => state.loading);

useEffect(() => {
        dispatch(getTasks())
    }, [dispatch]);


    const sendTask = (e) => {
        e.preventDefault();
        dispatch(sendTasks());
    };


    const onInputChange = (e) => {
        dispatch(changeInput(e))
    };

    const deleteTask = (id) => {
        dispatch(removeTask(id));
    };


    return (
        <div>
            {loadingSpinner ? <Spinner/> : null}

            <h1>Add Task</h1>
            <form
                className='TaskForm'
                onSubmit={e => sendTask(e)}
            >
                <input className='form'
                       type='text'
                       value={task1}
                       onChange={e => onInputChange(e)}
                />
                <button type='submit' className='formButton'>Add</button>
            </form>
            {tasks1.map(el => (
                <div className='Task'
                     key={el.id}
                     id={el.id}
                >
                    <p>{el.task}</p>
                    <button onClick={() => deleteTask(el.id)} className='deleteButton'><img src={deleteIcon} alt='deleteIcon' width='30px'/></button>
                </div>
            ))}
        </div>
    )


};

export default TaskApp;